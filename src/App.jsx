import React, { Component } from "react";
import "./App.css";

const myConstraints = {
  audio: false,
  video: true
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mediaStream: {},
      status: "play",
      mediaRecorder: {},
      chunks: [],
      videos: [],
      show: true,
      container: null,
      slides: [],
      container2: null,
      slides2: [],
      current: 0,
      msg: "START",
      showvideos: false,
      stateclass: "video1",
      recordclass: "video2",
      mensaje: "play"
    };
    this.vref = React.createRef();
    this.vref2 = React.createRef();

    this.handleRecord = this.handleRecord.bind(this);
    this.fullBleedVideo = React.createRef();

    this.videoref = React.createRef();
  }

  componentDidMount() {
    this.getMediaStram();
    this.getData();
  }

  async getMediaStram() {
    var mediaStream = await navigator.mediaDevices.getUserMedia(myConstraints);
    if (mediaStream.active) {
      const mediaRecorder = new MediaRecorder(mediaStream);
      this.setState(
        {
          mediaStream: mediaStream,
          mediaRecorder: mediaRecorder
        },
        () => (this.videoref.current.srcObject = mediaStream)
      );
    }
  }

  handleRecord = () => {
    const { status, mediaRecorder } = this.state;

    const video = document.getElementById(this.state.stateclass);
    const video2 = document.getElementById(this.state.recordclass);

    if (status === "play") {
      video.play();
      mediaRecorder.start();
      this.setState({
        status: "stop",
        show: false,
        msg: "STOP"
      });
    } else {
      video.pause();
      mediaRecorder.stop();
      mediaRecorder.ondataavailable = async e => {
        await this.setState({
          chunks: e.data
        });
      };

      mediaRecorder.onstop = e => {
        const chunks = this.state.chunks;
        const blob = new Blob([chunks], { type: "vide/mp4;" });
        const videourl = window.URL.createObjectURL(blob);
        this.setState({
          videos: [...this.state.videos, videourl],
          chunks: [],
          status: "play",
          show: true,
          msg: "START"
        });
        video2.src = videourl;
      };
      this.state.show
        ? (video.style.display = "block")
        : (video.style.display = "none");
      this.state.show
        ? (video2.style.display = "none")
        : (video2.style.display = "block");
      this.state.show ? (video2.controls = false) : (video2.controls = true);
    }
  };

  getData = () => {
    const container = document.getElementById("box1");
    const slides = container.querySelectorAll(".clase");
    this.setState({
      container2: container,
      slides2: slides
    });
  };

  carouselSelect = e => {
    var m = e.target.id - 1;
    switch (m) {
      case 0:
        this.setState({
          stateclass: "video1",
          recordclass: "video2"
        });
        break;
      case 1:
        this.setState({
          stateclass: "video3",
          recordclass: "video4"
        });
        break;
      case 2:
        this.setState({
          stateclass: "video5",
          recordclass: "video6"
        });
        break;
      case 3:
        this.setState({
          stateclass: "video7",
          recordclass: "video8"
        });
        break;
      case 4:
        this.setState({
          stateclass: "video9",
          recordclass: "video10"
        });
        break;
    }

    for (let index = 0; index < this.state.slides2.length; index++) {
      if (index == m) {
        this.state.slides2[index].style.display = "block";
      } else {
        this.state.slides2[index].style.display = "none";
      }
    }
    console.log("slides", this.state.slides2);
  };
  showVideos = e => {
    this.setState({
      showvideos: true
    });
  };

  videoStart = () => {
    const m = this.state.mensaje;
    if (m === "play") {
      this.videoref.current.play();
      this.setState({
        mensaje: "stop"
      });
    } else if (m === "stop") {
      this.videoref.current.stop();
      this.setState({
        mensaje: "play"
      });
    }
  };

  render() {
    const videos = this.state.videos.map(i => {
      if (this.state.showvideos === true) {
        return <video controls className="video" controls src={i} />;
      } else {
        return null;
      }
    });
    const icon = () => {
      if (this.state.status === "play") {
        return <i class="material-icons">play_circle_filled</i>;
      } else {
        return <i class="material-icons">pause</i>;
      }
    };
    return (
      <div>
        <div className="header" />

        <div className="cont">
          <button id="1" onClick={this.carouselSelect}>
            INICIO
          </button>
          <div>
            {icon}
            <button onClick={this.handleRecord}>{this.state.msg}</button>
          </div>

          <div id="box1">
            <div className="clase" id="01">
              <h3>
                1: ¿Cúales son las primeras diferencias que encuentras en UX y
                UI?
              </h3>
              <video className="video" ref={this.fullBleedVideo} id="video1" />
              <video
                className="video2"
                ref={this.fullBleedVideo2}
                id="video2"
              />
              <button id="2" onClick={this.carouselSelect}>
                Pregunta 2
              </button>
            </div>
            <div className="clase" id="02">
              <h3>2: ¿En donde te ves en 5 años?</h3>
              <video className="video" ref={this.fullBleedVideo} id="video3" />
              <video
                className="video2"
                ref={this.fullBleedVideo2}
                id="video4"
              />
              <button id="3" onClick={this.carouselSelect}>
                Pregunta 3
              </button>
            </div>
            <div className="clase" id="03">
              <h3>3: ¿Cúales son tus metas?</h3>
              <video className="video" ref={this.fullBleedVideo} id="video5" />
              <video
                className="video2"
                ref={this.fullBleedVideo2}
                id="video6"
              />
              <button id="4" onClick={this.carouselSelect}>
                Pregunta 4
              </button>
            </div>
            <div className="clase" id="04">
              <h3>
                4: ¿Cúales son las primeras diferencias que encuentras en UX y
                UI?, has tenido experiencia
              </h3>
              <video className="video" ref={this.fullBleedVideo} id="video7" />
              <video
                className="video2"
                ref={this.fullBleedVideo2}
                id="video8"
              />
              <button id="5" onClick={this.carouselSelect}>
                Pregunta 5
              </button>
            </div>
            <div className="clase" id="05">
              <h3>
                5: ¿Cúales son las primeras diferencias que encuentras en UX y
                UI?
              </h3>
              <video className="video" ref={this.fullBleedVideo} id="video9" />
              <video
                className="video2"
                ref={this.fullBleedVideo2}
                id="video10"
              />
            </div>
          </div>

          <div>
            <h2>videos</h2>

            <button onClick={this.showVideos}>Mostrar Todos los videos</button>
            <div>{videos}</div>
          </div>
        </div>

        <div>
          <button onClick={this.videoStart}>apreta</button>
          <video ref={this.videoref} />
        </div>
      </div>
    );
  }
}

export default App;

// render() {
//   const data = this.props.datos.map((datas,index) => {
//     console.log('datas descragada',index)

//     return <EventCard  key={index} llave={index} datos={datas} />;
//   });
//   return <div className="card">{data}</div>;
// }
